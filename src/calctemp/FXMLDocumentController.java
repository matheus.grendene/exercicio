/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calctemp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author mathe <matheus2001gbg@gmail.com>
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label,label2;
    @FXML
    private TextField textfield;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        double num, Num;
        Num = Double.parseDouble(textfield.getText());
        num = (Num * 9/5) + 32;
        label.setText(num+"°F");
        num = Num+ + 273.15;
        label2.setText(num+"°K");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
